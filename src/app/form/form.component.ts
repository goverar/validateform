import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  exampleForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router, ) { }

  ngOnInit() {

  
    this.exampleForm = this.fb.group({
      'email': [null, Validators.required],
      'pass': [null, Validators.required],
      'date': [null, Validators.required]
    });

  }

  onSubmit({ value, valid }: { value: any, valid: boolean }) {
    console.log(value, valid);
    //this.router.navigate(['/detalle']);
  }

  resetFormValues(){
    this.exampleForm.reset();
  }

}
