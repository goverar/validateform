import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { DetalleComponent } from './detalle/detalle.component';


const routes: Routes = [
    
    { path: '', component: FormComponent },
    //{ path: 'form', component: FormComponent },
    { path: 'detalle', component: DetalleComponent },
    //{ path: '**', redirectTo: 'not-found' }
];
  
@NgModule({
    imports: [RouterModule.forRoot(routes, {'useHash': true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }